# simple-k8s-tf

This repo consists of two Terraform IaC files that will create a Kubernetes cluster.

## The Missing File

The only file that still needs to be configured in this repository is the `terraform.tfvars`. Please populate any of the missing variables before running `terraform apply`.


This was tested with:
```
Terraform v0.11.11
+ provider.google v1.20.0
```
